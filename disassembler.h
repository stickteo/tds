// disassembler.h

// *** instruction formats ***

struct form_mux{
	union{
		uint32_t val;
		struct{
			uint32_t bits_3_0:4, bits_7_4:4, bits_19_8:12, bit_20:1, bits_24_21:4, bits_27_25:3, bits_31_28:4;
		};
		struct{
			uint32_t :20, bits_24_20:5, :7;
		};
	};
};

struct form_data{
	union{
		uint32_t val;
		struct{
			uint32_t shifter_operand:12, rd:4, rn:4, s:1, opcode:4, i:1, :2, cond:4;
		};
		struct{
			uint32_t immed_8:8, rotate_imm:4, :20;
		};
		struct{
			uint32_t rm:4, :1, shift:2, shift_imm:5, :20;
		};
		struct{
			uint32_t :8, rs:4, :20;
		};
	};
};

struct form_psr{
	union{
		uint32_t val;
		struct{
			uint32_t immed_8:8, rotate_imm:4, rd:4, field_mask:4, :1, s:1, r:1, :2, i:1, :2, cond:4;
		};
		struct{
			uint32_t rm:4, :28;
		};
	};
};

struct form_mul{
	union{
		uint32_t val;
		struct{
			uint32_t rm:4, :4, rs:4, rn:4, rd:4, s:1, a:1, :6, cond:4;
		};
		struct{
			uint32_t :12, rdlo:4, rdhi:4, :1, sa:2, :9;
		};
	};
};

struct form_swp{
	union{
		uint32_t val;
		struct{
			uint32_t rm:4, :8, rd:4, rn:4, :2, b:1, :5, cond:4;
		};
	};
};

struct form_mem{
	union{
		uint32_t val;
		struct{
			uint32_t rm:4, :1, sh:2, :5, rd:4, rn:4, puiwl:5, :3, cond:4;
		};
		struct{
			uint32_t immedl:4, :1, h:1, s:1, :1, immedh:4, :8, l:1, w:1, i:1, u:1, p:1, :7;
		};
	};
};

// *** _ss : string arrays ***

const char cond_ss[16][3] = {
	"eq", "ne", "cs", "cc",
	"mi", "pl", "vs", "vc",
	"hi", "ls", "ge", "lt",
	"gt", "le", "al", "-"
};

const char dataop_ss[16][4] = {
	"and", "eor", "sub", "rsb",
	"add", "adc", "sbc", "rsc",
	"tst", "teq", "cmp", "cmn",
	"orr", "mov", "bic", "mvn"
};

const char setflag_ss[2][2] = {
	"", "s"
};

const char reg_ss[16][4] = {
	"r0", "r1", "r2", "r3",
	"r4", "r5", "r6", "r7",
	"r8", "r9", "r10", "r11",
	"r12", "sp", "lr", "pc"
};

const char shift_ss[4][4] = {
	"lsl", "lsr", "asr", "ror"
};

const char shift0_ss[4][10] = {
	"", ", lsr #32", ", asr #32", ", rrx"
};

const char psrop_ss[2][4] = {
	"mrs", "msr"
};

const char psrr_ss[2][5] = {
	"cpsr", "spsr"
};

const char psrfm_ss[16][5] = {
	"none", "c", "x", "xc",
	"s", "sc", "sx", "sxc",
	"f", "fc", "fx", "fxc",
	"fs", "fsc", "fsx", "fsxc"
};

const char mulop_ss[2][4] = {
	"mul", "mla"
};

const char mullop_ss[4][6] = {
	"umull", "umlal", "smull", "smlal"
};

const char b_ss[2][2] = {
	"", "b"
};

const char meml_ss[2][4] = {
	"str", "ldr"
};

// *** mux functions ***

// *** main branch *** (bits [27:25])
uint32_t mux_a(uint32_t);
uint32_t mux_b(uint32_t);
uint32_t mux_c(uint32_t);
uint32_t mux_d(uint32_t);
uint32_t mux_e(uint32_t);
uint32_t mux_f(uint32_t);
uint32_t mux_g(uint32_t);
uint32_t mux_h(uint32_t);
uint32_t (*mux_table_a[8])(uint32_t) = {
	mux_a, mux_b, mux_c, mux_d,
	mux_e, mux_f, mux_g, mux_h 
};

// *** branch a *** (bits [7:4])
// |    | 0             | 1                     | 2         | 3               |
// | -- | ------------- | --------------------- | --------  | --------------- |
// |  0 | data imm, psr | data reg, bx          | data imm  | data reg        |
// |  4 | data imm      | data reg              | data imm  | data reg        |
// |  8 | data imm      | mult, mult long, swap | data imm  | halfword        |
// | 12 | data imm      | signed byte           | data imm  | signed halfword |
//
uint32_t mux_i(uint32_t); // data imm, psr
uint32_t mux_j(uint32_t); // data reg, bx
uint32_t mux_k(uint32_t); // data imm
uint32_t mux_l(uint32_t); // data reg
uint32_t mux_m(uint32_t); // mult, mult long, swap
uint32_t mux_n(uint32_t); // halfword
uint32_t mux_o(uint32_t); // signed byte
uint32_t mux_p(uint32_t); // signed halfword
uint32_t (*mux_table_b[16])(uint32_t) = {
	mux_i, mux_j, mux_k, mux_l,
	mux_k, mux_l, mux_k, mux_l,
	mux_k, mux_m, mux_k, mux_n,
	mux_k, mux_o, mux_k, mux_p
};

// *** branch i *** (bits [24:21])
// and ands eor eors sub subs rsb rsbs
// add adds adc adcs sbc sbcs rsc rscs
// mrs tst  msr teq  mrs cmp  msr cmn
// orr orrs mov movs bic bics mvn mvns
uint32_t mux_q(uint32_t); // and eor sub rsb add adc sbc rsc orr bic
uint32_t mux_2(uint32_t); // mrs
uint32_t mux_u(uint32_t); // tst teq cmp cmn
uint32_t mux_3(uint32_t); // msr
uint32_t mux_s(uint32_t); // mov mvn
uint32_t (* const mux_table_f[32])(uint32_t) = {
	mux_q, mux_q, mux_q, mux_q, mux_q, mux_q, mux_q, mux_q,
	mux_q, mux_q, mux_q, mux_q, mux_q, mux_q, mux_q, mux_q,
	mux_2, mux_u, mux_3, mux_u, mux_2, mux_u, mux_3, mux_u,
	mux_q, mux_q, mux_s, mux_s, mux_q, mux_q, mux_s, mux_s
};

// *** branch j *** (bits [24:21]) // mostly the same as i
// and ands eor eors sub subs rsb rsbs
// add adds adc adcs sbc sbcs rsc rscs
// ??? tst  ??? teq  bx  cmp  ??? cmn
// orr orrs mov movs bic bics mvn mvns
uint32_t mux_v(uint32_t); // and eor sub rsb add adc sbc rsc orr bic
uint32_t mux_1(uint32_t);
uint32_t mux_z(uint32_t); // tst teq cmp cmn
uint32_t mux_y(uint32_t); // ? bx ? ?
uint32_t mux_x(uint32_t); // mov mvn
uint32_t (* const mux_table_e[32])(uint32_t) = {
	mux_v, mux_v, mux_v, mux_v, mux_v, mux_v, mux_v, mux_v,
	mux_v, mux_v, mux_v, mux_v, mux_v, mux_v, mux_v, mux_v,
	mux_1, mux_z, mux_y, mux_z, mux_1, mux_z, mux_1, mux_z,
	mux_v, mux_v, mux_x, mux_x, mux_v, mux_v, mux_x, mux_x
};

// *** branch k *** data reg imm (only)
// and ands eor eors sub subs rsb rsbs
// add adds adc adcs sbc sbcs rsc rscs
// ??? tst  ??? teq  ??? cmp  ??? cmn
// orr orrs mov movs bic bics mvn mvns
uint32_t (* const mux_table_g[32])(uint32_t) = {
	mux_q, mux_q, mux_q, mux_q, mux_q, mux_q, mux_q, mux_q,
	mux_q, mux_q, mux_q, mux_q, mux_q, mux_q, mux_q, mux_q,
	mux_1, mux_u, mux_1, mux_u, mux_1, mux_u, mux_1, mux_u,
	mux_q, mux_q, mux_s, mux_s, mux_q, mux_q, mux_s, mux_s
};

// *** branch l *** data reg reg (only)
// and ands eor eors sub subs rsb rsbs
// add adds adc adcs sbc sbcs rsc rscs
// ??? tst  ??? teq  ??? cmp  ??? cmn
// orr orrs mov movs bic bics mvn mvns
uint32_t (* const mux_table_h[32])(uint32_t) = {
	mux_v, mux_v, mux_v, mux_v, mux_v, mux_v, mux_v, mux_v,
	mux_v, mux_v, mux_v, mux_v, mux_v, mux_v, mux_v, mux_v,
	mux_1, mux_z, mux_1, mux_z, mux_1, mux_z, mux_1, mux_z,
	mux_v, mux_v, mux_x, mux_x, mux_v, mux_v, mux_x, mux_x
};

// *** branch m *** mul, mul long, swp
// mul   muls   mla   mlas   ????? ?????? ????? ??????
// umull umulls umlal umlals smull smulls smlal smlals
// swp   ?????? ????? ?????? swpb  ?????? ????? ??????
// ????? ?????? ????? ?????? ????? ?????? ????? ??????
// 0000xb mul
// 0001xb mla
// 0100xb umull
// 0101xb umlal
// 0110xb smull
// 0111xb smlal
// 10000b swp
// 10100b swpb
uint32_t mux_4(uint32_t); // mul
uint32_t mux_7(uint32_t); // mla
uint32_t mux_5(uint32_t); // umull, umlal, smull, smlal
uint32_t mux_6(uint32_t); // swp, swpb
uint32_t (* const mux_table_i[32])(uint32_t) = {
	mux_4, mux_4, mux_7, mux_7, mux_1, mux_1, mux_1, mux_1,
	mux_5, mux_5, mux_5, mux_5, mux_5, mux_5, mux_5, mux_5,
	mux_6, mux_1, mux_1, mux_1, mux_6, mux_1, mux_1, mux_1,
	mux_1, mux_1, mux_1, mux_1, mux_1, mux_1, mux_1, mux_1
};
