// disassembler.c
#include <stdint.h>
#include <stdio.h>
#include "disassembler.h"

//This comment is sponsored by Nikhil Ayyala

void print_dataop_1(struct form_data inst){
	// <opcode1>{<cond>}{S} <Rd>, <shifter_operand>
	printf("%s%s%s ", dataop_ss[inst.opcode], cond_ss[inst.cond], setflag_ss[inst.s]);
	printf("%s, ", reg_ss[inst.rd]);
}

void print_dataop_2(struct form_data inst){
	// <opcode2>{<cond>} <Rn>, <shifter_operand>
	printf("%s%s %s, ", dataop_ss[inst.opcode], cond_ss[inst.cond], reg_ss[inst.rn]);
}

void print_dataop_3(struct form_data inst){
	// <opcode3>{<cond>}{S} <Rd>, <Rn>, <shifter_operand>
	printf("%s%s%s ", dataop_ss[inst.opcode], cond_ss[inst.cond], setflag_ss[inst.s]);
	printf("%s, %s, ", reg_ss[inst.rd], reg_ss[inst.rn]);
}

void print_shifter_reg_imm(struct form_data inst){
	// <Rm> // <Rm>, LSL #<shift_imm> // <Rm>, LSR #<shift_imm>
	// <Rm>, ASR #<shift_imm>         // <Rm>, RRX // <Rm>, ROR #<shift_imm>
	printf("%s", reg_ss[inst.rm]);
	if(!inst.shift_imm){
		printf("%s", shift0_ss[inst.shift]);
	}else{
		printf(", %s #%d", shift_ss[inst.shift], inst.shift_imm);
	}
}

void print_shifter_reg_reg(struct form_data inst){
	// <Rm>, LSL <Rs> // <Rm>, LSR <Rs>
	// <Rm>, ASR <Rs> // <Rm>, ROR <Rs>
	printf("%s, %s %s", reg_ss[inst.rm], shift_ss[inst.shift], reg_ss[inst.rs]);
}

void print_addrmode3(struct form_mem inst){
	switch(inst.puiwl>>1){
		case  0: // 0000 : post reg -
			printf("], -%s", reg_ss[inst.rm]); break;
		case  1: // 0001 : unpredictable
			break;
		case  2: // 0010 : post imm -
			printf("], -#%d", inst.immedh<<4 + inst.immedl); break;
		case  3: // 0011 : unpredictable
			break;
		case  4: // 0100 : post reg +
			printf("], +%s", reg_ss[inst.rm]); break;
		case  5: // 0101 : unpredictable
			break;
		case  6: // 0110 : post imm +
			printf("], +#%d", inst.immedh<<4 + inst.immedl); break;
		case  7: // 0111 : unpredictable
			break;
		case  8: // 1000 : offset reg -
			printf(", -%s]", reg_ss[inst.rm]); break;
		case  9: // 1001 : pre reg -
			printf(", -%s]!", reg_ss[inst.rm]); break;
		case 10: // 1010 : offset imm -
			printf(", -#%d]", inst.immedh<<4 + inst.immedl); break;
		case 11: // 1011 : pre imm -
			printf(", -#%d]!", inst.immedh<<4 + inst.immedl); break;
		case 12: // 1100 : offset reg +
			printf(", +%s]", reg_ss[inst.rm]); break;
		case 13: // 1101 : pre reg +
			printf(", +%s]!", reg_ss[inst.rm]); break;
		case 14: // 1110 : offset imm +
			printf(", +#%d]", inst.immedh<<4 + inst.immedl); break;
		default: // 1111 : pre imm +
			printf(", +#%d]!", inst.immedh<<4 + inst.immedl); break;
	}
}

uint32_t decode(uint32_t opcode){
	struct form_mux mux;
	mux.val = opcode;
	mux_table_a[mux.bits_27_25](opcode);
}

uint32_t mux_a(uint32_t opcode){
	struct form_mux mux;
	mux.val = opcode;
	mux_table_b[mux.bits_7_4](opcode);
}
	uint32_t mux_i(uint32_t opcode){
		struct form_mux mux;
		mux.val = opcode;
		//mux_table_c[mux.bits_24_21](opcode);
		mux_table_f[mux.bits_24_20](opcode);
	}
		uint32_t mux_q(uint32_t opcode){
			struct form_data inst;
			inst.val = opcode;
			print_dataop_3(inst);
			print_shifter_reg_imm(inst);
		}
		uint32_t mux_2(uint32_t opcode){
			// MRS{<cond>} <Rd>, CPSR
			// MRS{<cond>} <Rd>, SPSR
			struct form_psr inst;
			inst.val = opcode;
			printf("mrs%s %s, %s", cond_ss[inst.cond], reg_ss[inst.rd], psrr_ss[inst.r]);
		}
		uint32_t mux_u(uint32_t opcode){
			struct form_data inst;
			inst.val = opcode;
			print_dataop_2(inst);
			print_shifter_reg_imm(inst);
		}
		uint32_t mux_3(uint32_t opcode){
			// MSR{<cond>} CPSR_<fields>, <Rm>
			// MSR{<cond>} SPSR_<fields>, <Rm>
			struct form_psr inst;
			inst.val = opcode;
			if(!inst.field_mask){
				printf(".word 0x%08x @ ", opcode);
			}
			printf("msr%s %s_%s, %s", cond_ss[inst.cond], psrr_ss[inst.r], psrfm_ss[inst.field_mask], reg_ss[inst.rm]);
		}
		uint32_t mux_s(uint32_t opcode){
			struct form_data inst;
			inst.val = opcode;
			print_dataop_1(inst);
			print_shifter_reg_imm(inst);
		}
	uint32_t mux_j(uint32_t opcode){
		struct form_mux mux;
		mux.val = opcode;
		mux_table_e[mux.bits_24_20](opcode);
	}
		uint32_t mux_v(uint32_t opcode){
			struct form_data inst;
			inst.val = opcode;
			print_dataop_3(inst);
			print_shifter_reg_reg(inst);
		}
		uint32_t mux_1(uint32_t opcode){
			printf(".word 0x%08x", opcode); // undefined ?
		}
		uint32_t mux_z(uint32_t opcode){
			struct form_data inst;
			inst.val = opcode;
			print_dataop_2(inst);
			print_shifter_reg_reg(inst);
		}
		uint32_t mux_y(uint32_t opcode){
			struct form_data inst;
			inst.val = opcode;
			printf("bx%s %s", cond_ss[inst.cond], reg_ss[inst.rm]);
		}
		uint32_t mux_x(uint32_t opcode){
			struct form_data inst;
			inst.val = opcode;
			print_dataop_1(inst);
			print_shifter_reg_reg(inst);
		}
	uint32_t mux_k(uint32_t opcode){
		struct form_mux mux;
		mux.val = opcode;
		mux_table_g[mux.bits_24_20](opcode);
	}
	uint32_t mux_l(uint32_t opcode){
		struct form_mux mux;
		mux.val = opcode;
		mux_table_h[mux.bits_24_20](opcode);
	}
	uint32_t mux_m(uint32_t opcode){
		struct form_mux mux;
		mux.val = opcode;
		mux_table_i[mux.bits_24_20](opcode);
	}
		uint32_t mux_4(uint32_t opcode){
			// MUL{<cond>}{S} <Rd>, <Rm>, <Rs>
			struct form_mul inst;
			inst.val = opcode;
			printf("%s%s%s ", mulop_ss[inst.a], cond_ss[inst.cond], setflag_ss[inst.s]);
			printf("%s, %s, %s", reg_ss[inst.rd], reg_ss[inst.rm], reg_ss[inst.rs]);
		}
		uint32_t mux_7(uint32_t opcode){
			// MLA{<cond>}{S} <Rd>, <Rm>, <Rs>, <Rn>
			struct form_mul inst;
			inst.val = opcode;
			printf("%s%s%s ", mulop_ss[inst.a], cond_ss[inst.cond], setflag_ss[inst.s]);
			printf("%s, %s, %s, %s", reg_ss[inst.rd], reg_ss[inst.rm], reg_ss[inst.rs], reg_ss[inst.rn]);
		}
		uint32_t mux_5(uint32_t opcode){
			// UMULL{<cond>}{S} <RdLo>, <RdHi>, <Rm>, <Rs>
			// UMLAL{<cond>}{S} <RdLo>, <RdHi>, <Rm>, <Rs>
			// SMULL{<cond>}{S} <RdLo>, <RdHi>, <Rm>, <Rs>
			// SMLAL{<cond>}{S} <RdLo>, <RdHi>, <Rm>, <Rs>
			struct form_mul inst;
			inst.val = opcode;
			printf("%s%s%s ", mullop_ss[inst.sa], cond_ss[inst.cond], setflag_ss[inst.s]);
			printf("%s, %s, %s, %s", reg_ss[inst.rdlo], reg_ss[inst.rdhi], reg_ss[inst.rm], reg_ss[inst.rs]);
		}
		uint32_t mux_6(uint32_t opcode){
			// SWP{<cond>} <Rd>, <Rm>, [<Rn>]
			// SWP{<cond>}B <Rd>, <Rm>, [<Rn>]
			struct form_swp inst;
			inst.val = opcode;
			printf("swp%s%s ", cond_ss[inst.cond], b_ss[inst.b]);
			printf("%s, %s, [%s]", reg_ss[inst.rd], reg_ss[inst.rm], reg_ss[inst.rn]);
		}
	uint32_t mux_n(uint32_t opcode){
		// STR{<cond>}H <Rd>, <addressing_mode>
		// LDR{<cond>}H <Rd>, <addressing_mode>
		struct form_mem inst;
		inst.val = opcode;
		if(!inst.p && inst.w){ // unpredictable (pw = 01)
			printf(".word 0x%08x", opcode);
			return 0;
		}
		printf("%s%sh %s, ", meml_ss[inst.l], cond_ss[inst.cond], reg_ss[inst.rd]);
		printf("[%s", reg_ss[inst.rn]);
		print_addrmode3(inst);
	}
	uint32_t mux_o(uint32_t opcode){
		// STR{<cond>}SB <Rd>, <addressing_mode>
		// LDR{<cond>}SB <Rd>, <addressing_mode>
		struct form_mem inst;
		inst.val = opcode;
		if(!inst.p && inst.w){ // unpredictable (pw = 01)
			printf(".word 0x%08x", opcode);
			return 0;
		}
		printf("%s%ssb %s, ", meml_ss[inst.l], cond_ss[inst.cond], reg_ss[inst.rd]);
		printf("[%s", reg_ss[inst.rn]);
		print_addrmode3(inst);
	}
	uint32_t mux_p(uint32_t opcode){
		// STR{<cond>}SH <Rd>, <addressing_mode>
		// LDR{<cond>}SH <Rd>, <addressing_mode>
		struct form_mem inst;
		inst.val = opcode;
		if(!inst.p && inst.w){ // unpredictable (pw = 01)
			printf(".word 0x%08x", opcode);
			return 0;
		}
		printf("%s%ssh %s, ", meml_ss[inst.l], cond_ss[inst.cond], reg_ss[inst.rd]);
		printf("[%s", reg_ss[inst.rn]);
		print_addrmode3(inst);
	}

uint32_t mux_b(uint32_t opcode){}
uint32_t mux_c(uint32_t opcode){}
uint32_t mux_d(uint32_t opcode){}
uint32_t mux_e(uint32_t opcode){}
uint32_t mux_f(uint32_t opcode){}
uint32_t mux_g(uint32_t opcode){}
uint32_t mux_h(uint32_t opcode){}

void main2(void){
	uint32_t i, code;

	printf("\n*** 1. DECODE 12 BITS: [27:20] [7:4] ***\n");
	for(i=0; i<512; i++){
		// mapping i[3:0] to code[7:4]
		// and i[11:4] to code[27:20]
		code = ((i>>4)<<20) | ((i&15)<<4);
		printf("%03x: ", i);
		decode(code);
		printf("\n");
	}

	printf("\n*** 2. CONDITIONALS ***\n");
	for(i=0; i<16; i++){
		code = i<<28;
		printf("%x: ", i);
		decode(code);
		printf("\n");
	}
}

void main(void){
	uint32_t i, code;

	for(i=0; i<512; i++){
		// mapping i[3:0] to code[7:4]
		// and i[11:4] to code[27:20]
		code = ((i>>4)<<20) | ((i&15)<<4);
		//printf("%03x: ", i);
		decode(code);
		printf("\n");
	}
}