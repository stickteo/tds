// arm7.c
// created   : 2018 november 21
// last edit : 2018 december 19
// stickteo
// log
// 2018 december 19 - decode based on assembler labels instead
// 2018 november 23 - finished decode for arm instructions
// 2018 november 24
// work on alternative way to decode inst by using addressing modes
// get the general feel of decoding using the addressing modes
// todo: generate a lookup table using bits [27:20] and [7:4] (12 bits)
// 2018 november 25
// finished outline for modes 1 - 5

#include <stdint.h>
#include "arm7.h"

uint32_t mem_load(struct memory *mem, uint32_t addr){
	// todo : temporary... need to create memory manager
	return 0;
}

const uint32_t reg_mode[16] = { // maps mode bits to 6 register bank types
	0, 1, 2, 3, 0, 0, 0, 4, 0, 0, 0, 5, 0, 0, 0, 0
};

const uint32_t reg_bank[6][16] = { // maps 16 reg values to 31 banked values
	{0, 1, 2, 3, 4, 5, 6, 7,  8,  9, 10, 11, 12, 13, 14, 15}, // usr / sys
	{0, 1, 2, 3, 4, 5, 6, 7, 16, 17, 18, 19, 20, 21, 22, 15}, // fiq
	{0, 1, 2, 3, 4, 5, 6, 7,  8,  9, 10, 11, 12, 23, 24, 15}, // irq
	{0, 1, 2, 3, 4, 5, 6, 7,  8,  9, 10, 11, 12, 25, 26, 15}, // svc
	{0, 1, 2, 3, 4, 5, 6, 7,  8,  9, 10, 11, 12, 27, 28, 15}, // abt
	{0, 1, 2, 3, 4, 5, 6, 7,  8,  9, 10, 11, 12, 29, 30, 15}, // und
};

uint32_t reg_load(struct arm *core, uint32_t reg){
	return core->regs[reg_bank[core->m & 0xF][reg]];
}

void reg_store(struct arm *core, uint32_t reg, uint32_t val){
	core->regs[reg_bank[core->m & 0xF][reg]] = val;
}

const uint32_t spsr_mode[16] = { // mapping for spsr
	0, 1, 2, 3, 0, 0, 0, 4, 0, 0, 0, 5, 0, 0, 0, 0
};

uint32_t reg_load_spsr(struct arm *core){
	return core->psr[spsr_mode[core->m & 0xF]];
	// note: for now, loading spsr in user or sys mode will return cpsr
}

void reg_store_spsr(struct arm *core, uint32_t val){
	core->psr[spsr_mode[core->m & 0xF]] = val;
	// note: storing spsr in user or sys mode will store to cpsr
}

void fetch(struct arm *core, struct memory *mem){
	// todo : timing, for now just fetch
	core->f_s.inst = mem_load(mem, core->pc);
}

void decode_arm(struct arm *core){
	uint32_t inst, code, decode;

	inst = core->d_s.inst;
	code = (((inst>>20)&0xFF)<<4) | ((inst>>4)&0xF);
	decode = decode_table[code];
	
	switch(decode){
		case AND: case EOR:	case SUB: case RSB:
		case ADD: case ADC: case SBC: case RSC:
		case TST: case TEQ: case CMP: case CMN:
		case ORR: case MOV:	case BIC: case MVN:
			core->d_s.format = FORMAT_DATA;
			break;
		case MRS:
		case MSR:
			core->d_s.format = FORMAT_PSR;
			break;
		case MUL:
		case MLA:
			core->d_s.format = FORMAT_MULTIPLY;
			break;
		case UMULL:
		case UMLAL:
		case SMULL:
		case SMLAL:
			break;
		case SWP:
		case SWPB:
			break;
		case BX:
			break;
		case STRH:
		case LDRH:
		case LDRSB:
		case LDRSH:
			break;
		case STR:
		case STRB:
		case STRT:
		case STRBT:
		case LDR:
		case LDRB:
		case LDRT:
		case LDRBT:
			break;
		case UND:
			// todo : undefined exception
			break;
		case STM:
		case LDM:
			break;
		case B:
		case BL:
			break;
		case STC:
		case LDC:
			break;
		case CDP:
			break;
		case MCR:
		case MRC:
			break;
		case SWI:
			// cpu doesn't do anything with the 24bit immediate
			break;
		case UNK:
			// todo : undefined (probably)
			break;
		default:
			// error ! something bad happened !
			break;
	}
}

void decode_thumb(struct arm *core){
	uint32_t inst, code, decode;

	inst = core->d_s.inst;
	code = (inst>>6)&0x3FF;
	decode = decode_table_thumb[code];

	switch(decode){
		case TLSL1:
		case TLSR1:
		case TASR1:
		case TSTR1:
		case TLDR1:
		case TSTRB1:
		case TLDRB1:
		case TSTRH1:
		case TLDRH1:
			break;
		case TADD3:
		case TSUB3:
		case TADD1:
		case TMOV2:
		case TSUB1:
		case TSTR2:
		case TSTRB2:
		case TLDR2:
		case TLDRB2:
		case TSTRH2:
		case TLDRSB:
		case TLDRH2:
		case TLDRSH:
			break;
		case TMOV1:
		case TCMP1:
		case TADD2:
		case TSUB2:
		case TLDR3:
		case TSTR3:
		case TLDR4:
		case TADD5:
		case TADD6:
		case TSTMIA:
		case TLDMIA:
			break;
		case TAND:
		case TEOR:
		case TLSL2:
		case TLSR2:
		case TASR2:
		case TADC:
		case TSBC:
		case TROR:
		case TTST:
		case TNEG:
		case TCMP2:
		case TCMN:
		case TMUL:
		case TBIC:
		case TMVN:
			break;
		case TADD4:
		case TCMP3:
		case TMOV3:
		case TBX:
			break;
		case TADD7:
		case TSUB4:
			break;
		case TPUSH:
		case TPOP:
			break;
		case TB1:
			break;
		case TSWI:
			break;
		case TB2:
			break;
		case TBL:
			break;
		case TUND:
			// todo : undefined exception
			break;
		case TUNK:
			// todo : undefined exception (probably)
			break;
		default:
			// serious error !
			break;
	}
}

void decode(struct arm *core){
	if(core->t){ // decode thumb instead
		decode_thumb(core);
	}else{
		decode_arm(core);
	}
}

uint32_t execute_check_condition(uint32_t cond, uint32_t flags){
	uint32_t eq(uint32_t f){ return (f & 0x4) == 0x4; }
	uint32_t ne(uint32_t f){ return (f & 0x4) == 0x0; }
	uint32_t cs_hs(uint32_t f){ return (f & 0x2) == 0x2; }
	uint32_t cc_lo(uint32_t f){ return (f & 0x2) == 0x0; }
	uint32_t mi(uint32_t f){ return (f & 0x8) == 0x8; }
	uint32_t pl(uint32_t f){ return (f & 0x8) == 0x0; }
	uint32_t vs(uint32_t f){ return (f & 0x1) == 0x1; }
	uint32_t vc(uint32_t f){ return (f & 0x1) == 0x0; }
	uint32_t hi(uint32_t f){ return (f & 0x6) == 0x2; }
	uint32_t ls(uint32_t f){ return (f & 0x6) == 0x4; }
	uint32_t ge(uint32_t f){ return ((f & 0x8) >> 3) == (f & 0x1); }
	uint32_t lt(uint32_t f){ return ((f & 0x8) >> 3) != (f & 0x1); }
	uint32_t gt(uint32_t f){ return (((f & 0x8) >> 3) == (f & 0x1)) && ((f & 0x4) == 0x0); }
	uint32_t le(uint32_t f){ return (((f & 0x8) >> 3) != (f & 0x1)) && ((f & 0x4) == 0x4); }
	uint32_t al(uint32_t f){ return 1; }
	uint32_t un(uint32_t f){ return 1; }

	uint32_t (* const func[16]) (uint32_t f) = {
		eq, ne, cs_hs, cc_lo, mi, pl, vs, vc, hi, ls, ge, lt, gt, le, al, un
	};

	return func[cond](flags);
}

void execute_shifter(struct arm *core){
	uint32_t c_flag;
	uint32_t cpu_mode;
	uint32_t shift_type;

	uint32_t shift_out, carry_out;

	// shift functions
	uint32_t shift_none(uint32_t r, uint32_t s) { return r; }
	uint32_t shift_ror(uint32_t r, uint32_t s){
		s = s & 0x1F;
		return (r << s) | (r >> (32 - s));
	}
	uint32_t shift_zero(uint32_t r, uint32_t s) { return 0; }
	uint32_t shift_sign(uint32_t r, uint32_t s) { return 0 - (r >> 31); }
	uint32_t shift_rrx(uint32_t r, uint32_t s)  { return (c_flag << 31) | (r >> 1); }
	uint32_t shift_lsl(uint32_t r, uint32_t s)  { return r << s; }
	uint32_t shift_lsr(uint32_t r, uint32_t s)  { return r >> s; }
	uint32_t shift_asr(uint32_t r, uint32_t s){
		return ((r + 0x80000000) >> s) - (0x80000000 >> s);
	}

	// carry
	uint32_t carry_flag(uint32_t r, uint32_t s) { return c_flag;	}
	uint32_t carry_s_1(uint32_t r, uint32_t s)  { return (r >> ((s-1) & 0x1F)) & 0x1; }
	uint32_t carry_r31(uint32_t r, uint32_t s)  { return r >> 31; }
	uint32_t carry_r0(uint32_t r, uint32_t s)   { return r & 0x1; }
	uint32_t carry_32_s(uint32_t r, uint32_t s) { return (r >> (32 - s)) & 0x1; }
	uint32_t carry_zero(uint32_t r, uint32_t s) { return 0; }

	// shift types
	void shift_imm(void){
		uint32_t r, s, t;

		uint32_t (* const shift[2]) (uint32_t, uint32_t) = {shift_none, shift_ror};
		uint32_t (* const carry[2]) (uint32_t, uint32_t) = {carry_flag, carry_s_1};

		r = core->e_s.f_data.immed_8;
		s = core->e_s.f_data.rotate_imm * 2;
		t = (s>0);

		shift_out = shift[t](r, s);
		carry_out = carry[t](r, s);
	}
	void shift_reg_imm(void){
		uint32_t r, s, t, u;

		uint32_t (* const shift[2][4]) (uint32_t, uint32_t) = {
			{shift_none, shift_zero, shift_sign, shift_rrx},
			{shift_lsl , shift_lsr , shift_asr , shift_ror} // s > 0
		};
		uint32_t (* const carry[2][4]) (uint32_t, uint32_t) = {
			{carry_flag, carry_r31 , carry_r31 , carry_r0 },
			{carry_32_s, carry_s_1 , carry_s_1 , carry_s_1}
		};

		r = reg_load(core, core->e_s.f_data.rm);
		s = core->e_s.f_data.shift_imm;
		t = (s>0);
		u = core->e_s.f_data.shift;

		shift_out = shift[t][u](r, s);
		carry_out = carry[t][u](r, s);
	}
	void shift_reg_reg(void){
		uint32_t r, s, t, u;

		uint32_t (* const shift[4][4]) (uint32_t, uint32_t) = {
			{shift_none, shift_none, shift_none, shift_none}, // s == 0
			{shift_lsl , shift_lsr , shift_asr , shift_ror }, // s < 32
			{shift_zero, shift_zero, shift_sign, shift_none}, // s == 32
			{shift_zero, shift_zero, shift_sign, shift_ror}
		};
		uint32_t (* const carry[4][4]) (uint32_t, uint32_t) = {
			{carry_flag, carry_flag, carry_flag, carry_flag},
			{carry_32_s, carry_s_1 , carry_s_1 , carry_s_1 },
			{carry_r0  , carry_r31 , carry_r31 , carry_r31 },
			{carry_zero, carry_zero, carry_r31 , carry_s_1 }
		};

		r = reg_load(core, core->e_s.f_data.rm);
		s = reg_load(core, core->e_s.f_data.rs);
		t = (s>0) + (s>32) + (s>33);
		u = core->e_s.f_data.shift;

		shift_out = shift[t][u](r, s);
		carry_out = carry[t][u](r, s);
	}

	// execute mux
	void (* const type[4])(void) = {shift_reg_imm, shift_reg_reg, shift_imm, shift_imm};

	// execute
	c_flag = core->c;
	cpu_mode = core->m & 0xF;
	shift_type = core->e_s.f_data.i * 2 + core->e_s.f_data.bit4;

	type[shift_type]();

	core->e_s.shifter_out = shift_out;
	core->e_s.shifter_carry = carry_out;
}

void execute_alu(struct arm *core){
	uint32_t rn, s_in, s_cin, c_in;
	uint32_t op_mux, set_mux, pc_mux;
	uint32_t rd;
	uint64_t rd64;
	uint32_t n, z, c, v;

	void and(void){ rd = rn & s_in; }
	void eor(void){ rd = rn ^ s_in; }
	void sub(void){ rd64 = (uint64_t) rn - s_in;         rd = (uint32_t) rd64; }
	void rsb(void){ rd64 = (uint64_t) s_in - rn;         rd = (uint32_t) rd64; }
	void add(void){ rd64 = (uint64_t) rn + s_in;         rd = (uint32_t) rd64; }
	void adc(void){ rd64 = (uint64_t) rn + s_in + c_in;  rd = (uint32_t) rd64; }
	void sbc(void){ rd64 = (uint64_t) rn - s_in - !c_in; rd = (uint32_t) rd64; }
	void rsc(void){ rd64 = (uint64_t) s_in - rn - !c_in; rd = (uint32_t) rd64; }
	// void tst(void){ rd = rn & s_in; }
	// void teq(void){ rd = rn ^ s_in; }
	// void cmp(void){ rd64 = (uint64_t) rn - s_in;         rd = (uint32_t) rd64; }
	// void cmn(void){ rd64 = (uint64_t) rn + s_in;         rd = (uint32_t) rd64; }
	void orr(void){ rd = rn | s_in; }
	void mov(void){ rd = s_in; }
	void bic(void){ rd = rn & ~s_in; }
	void mvn(void){ rd = ~s_in; }
	void (* const operator[16])(void) = {
		and, eor, sub, rsb,
		add, adc, sbc, rsc,
		and, eor, sub, add,
		orr, mov, bic, mvn
	};

	uint32_t c_shift(void){ return s_cin; }
	uint32_t c_not_borrow(void){ return (~rd64 >> 32) & 0x1; }
	uint32_t c_carry(void){ return (rd64 >> 32) & 0x1; }
	uint32_t (* const c_flag[16])(void) = {
		c_shift, c_shift, c_not_borrow, c_not_borrow,
		c_carry, c_carry, c_not_borrow, c_not_borrow,
		c_shift, c_shift, c_not_borrow, c_carry,
		c_shift, c_shift, c_shift, c_shift
	};

	uint32_t v_same(void){ return core->v; }
	uint32_t v_overflow(void){ return (rd ^ rn ^ s_in) >> 31; }
	uint32_t (* const v_flag[16])(void) = {
		v_same, v_same, v_overflow, v_overflow,
		v_overflow, v_overflow, v_overflow, v_overflow,
		v_same, v_same, v_overflow, v_overflow,
		v_same, v_same, v_same, v_same
	};

	void set_none(void){ ; }
	void set_flag(void){
		core->n = (rd >> 31) & 0x1;
		core->z = (rd == 0);
		core->c = c_flag[op_mux]();
		core->v = v_flag[op_mux]();
	}
	void set_spsr(void){
		core->cpsr = reg_load_spsr(core);
		// note: spsr is undefined during user or sys mode
	}
	void (* const set_type[3][16])(void)= {
		{
			set_none, set_none, set_none, set_none,
			set_none, set_none, set_none, set_none,
			set_none, set_none, set_none, set_none,
			set_none, set_none, set_none, set_none
		},{
			set_flag, set_flag, set_flag, set_flag,
			set_flag, set_flag, set_flag, set_flag,
			set_flag, set_flag, set_flag, set_flag,
			set_flag, set_flag, set_flag, set_flag
		},{
			set_spsr, set_spsr, set_spsr, set_spsr,
			set_spsr, set_spsr, set_spsr, set_spsr,
			set_flag, set_flag, set_flag, set_flag,
			set_spsr, set_spsr, set_spsr, set_spsr
		}
	};

	void rd_none(void){ ; }
	void rd_store(void){
		reg_store(core, core->e_s.f_data.rd, rd);
	}
	void (* const rd_write[16])(void) = {
		rd_store, rd_store, rd_store, rd_store,
		rd_store, rd_store, rd_store, rd_store,
		rd_none, rd_none, rd_none, rd_none,
		rd_store, rd_store, rd_store, rd_store
	};

	// execute
	rn = reg_load(core, core->e_s.f_data.rn);
	s_in = core->e_s.shifter_out;
	s_cin = core->e_s.shifter_carry;
	c_in = core->c;

	op_mux = core->e_s.f_data.opcode;
	set_mux = core->e_s.f_data.s +
	          (core->e_s.f_data.s && (core->e_s.f_data.rd == 15));

	operator[op_mux]();
	rd_write[op_mux]();
	set_type[set_mux][op_mux]();
}

void execute_psr(struct arm *core){
	void mrs(void){
		uint32_t rd;
		switch(core->e_s.f_psr.r){
			case 0:  rd = core->cpsr; break;
			default: rd = reg_load_spsr(core); break;
		}
		reg_store(core, core->e_s.f_psr.rd, rd);

		// todo : unpredictable when rd==pc
	}
	void msr(void){
		uint32_t operand, field_mask;

		const uint32_t unalloc_mask = 0x0FFFFF00;
		const uint32_t byte_mask[16] = {
			0x00000000, 0x000000FF, 0x0000FF00, 0x0000FFFF,
			0x00FF0000, 0x00FF00FF, 0x00FFFF00, 0x00FFFFFF,
			0xFF000000, 0xFF0000FF, 0xFF00FF00, 0xFF00FFFF,
			0xFFFF0000, 0xFFFF00FF, 0xFFFFFF00, 0xFFFFFFFF
		};
		const uint32_t state_mask = 0x00000020;
		const uint32_t user_mask = 0xF0000000;
		const uint32_t priv_mask = 0x0000000F;

		void set_cpsr(void){
			uint32_t mask;
			uint32_t in_priv_mode, in_non_arm_state, mux;

			void cpsr_user(void){
				mask = byte_mask[field_mask] & user_mask;
			}
			void cpsr_priv_arm(void){
				mask = byte_mask[field_mask] & (user_mask | priv_mask);
			}
			void cpsr_priv_thumb(void){
				// todo : unpredictable
				mask = 0;
			}
			void (* const func[4])(void) = {
				cpsr_user, cpsr_user, cpsr_priv_arm, cpsr_priv_thumb
			};

			in_priv_mode = ((core->m & 0xF) != 0x0);
			in_non_arm_state = core->t;
			mux = in_priv_mode * 2 + in_non_arm_state;
			// 0: user arm, 1: user thumb, 2: priv arm, 3: priv thumb
			func[mux]();
			core->cpsr = (core->cpsr & ~mask) | (operand & mask);
		}
		void set_spsr(void){
			uint32_t mask, spsr;
			uint32_t mode_has_spsr;

			void spsr_none(void){
				// todo : unpredictable
			}
			void spsr_have(void){
				mask = byte_mask[field_mask] & (user_mask | priv_mask | state_mask);
				spsr = reg_load_spsr(core);
				reg_store_spsr(core, (spsr & ~mask) | (operand & mask));
			}
			void (* const func[2])(void) = {spsr_none, spsr_have};

			mode_has_spsr = ((core->m & 0xF) != 0x0) && ((core->m & 0xF) != 0xF);
			func[mode_has_spsr]();
		}
		void (* const func_r[2])(void) = {set_cpsr, set_spsr};

		execute_shifter(core);
		operand = core->e_s.shifter_out;
		field_mask = core->e_s.f_psr.field_mask;

		if(operand & unalloc_mask != 0){
			; // todo: unpredictable
		}

		func_r[core->e_s.f_psr.r]();
	}
	void (* const func_s[2])(void) = {mrs, msr}; // 0: load psr, 1: store psr

	func_s[core->e_s.f_psr.s]();
}

void execute_muliply(struct arm *core){
	uint32_t rm, rs, rn, rd;

	void mul(void){
		rd = rm * rs;
	}
	void mla(void){
		rn = reg_load(core, core->e_s.f_multiply.rn);
		rd = rm * rs + rn;
	}
	void (* const func_mul[2])(void) = {mul, mla};

	void flag_none(void){ ; }
	void flag_set(void){
		core->n = (rd >> 31);
		core->z = (rd == 0);
		// todo: unpredictable c_flag
		// v_flag unaffected
	}
	void (* const func_flag[2])(void) = {flag_none, flag_set};

	rm = reg_load(core, core->e_s.f_multiply.rm);
	rs = reg_load(core, core->e_s.f_multiply.rs);
	func_mul[core->e_s.f_multiply.a]();
	reg_store(core, core->e_s.f_multiply.rd, rd);
	func_flag[core->e_s.f_multiply.s]();
}

void execute(struct arm *core, struct memory *mem){
	switch(execute_check_condition(core->e_s.cond, core->flags)){
		case 1:
			break;
		default:
			return;
	}

	switch(core->e_s.format){
		case FORMAT_DATA:
			execute_shifter(core);
			execute_alu(core);
			break;
		case FORMAT_PSR:
			execute_psr(core);
			break;
		case FORMAT_MULTIPLY:
			execute_muliply(core);
			break;
	}
}

void latch(struct arm *core){
	// todo
};

void arm_run_cycle(struct arm *core, struct memory *mem){
	// todo : check for exceptions

	fetch(core, mem);
	decode(core);
	execute(core, mem);

	latch(core);
}

void main(void){
	// todo : remove
}
