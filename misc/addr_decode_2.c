
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

#define NUM_ARM7INST 50
#define FOREACH_ARM7INST(X) \
X(AND) X(EOR) X(SUB) X(RSB) X(ADD) X(ADC) X(SBC) X(RSC) \
X(TST) X(TEQ) X(CMP) X(CMN) X(ORR) X(MOV) X(BIC) X(MVN) \
X(MRS) X(MSR) \
X(MUL) X(MLA) \
X(UMULL) X(UMLAL) X(SMULL) X(SMLAL) \
X(SWP) X(SWPB) \
X(BX) \
X(STRH) X(LDRH) X(LDRSB) X(LDRSH) \
X(STR) X(STRB) X(STRT) X(STRBT) X(LDR) X(LDRB) X(LDRT) X(LDRBT) \
X(UND) \
X(STM) X(LDM) \
X(B) X(BL) \
X(STC) X(LDC) \
X(CDP) \
X(MCR) X(MRC) \
X(SWI) \
X(UNK)

enum ARM7INST_ENUM {
    FOREACH_ARM7INST(GENERATE_ENUM)
};

static const char *ARM7INST_STRING[] = {
    FOREACH_ARM7INST(GENERATE_STRING)
};

#define XOR_MASK(X,B,M,V) ((((X)>>(B))^(V))&(M))
#define E_MASK(W,X,Y,Z) (!XOR_MASK(W,X,Y,Z))

void p_decode_table(uint32_t *code){
	int i, c;

	printf("uint32_t decode_table[0x1000] = {");
	for(i=0; i<0x1000; i++){
		if(i%256 == 0){
			printf("\n\t// 0x%03x",i);
		}
		if(i%16 == 0){
			printf("\n\t");
		}
		printf("%-5s,", ARM7INST_STRING[code[i]]);
		
	}
	printf("\n};\n");
}

void decode(uint32_t *out){
	uint32_t i, j;
	uint32_t inst;
	uint32_t temp;
	uint32_t hot[NUM_ARM7INST];
	uint64_t mux;

	int count, count2;
	int a, b, c;

	printf("*** ARM DECODE ***\n");

	count = 0; count2 = 0;
	for(i=0; i<0x1000; i++){
		// inst[27:20] = i[11:4];
		// inst[7:4] = i[3:0];
		inst = (((i>>4)&0xFF)<<20) | ((i&0xF)<<4);

		temp = (
			(
				E_MASK(inst,25,0x7,0x0) && (
					E_MASK(inst,4,0x1,0x0) || E_MASK(inst,4,0x9,0x1)
				)
			) ||
			E_MASK(inst,25,0x7,0x1)
		);
		hot[AND] = temp && E_MASK(inst,21,0xF,0x0);
		hot[EOR] = temp && E_MASK(inst,21,0xF,0x1);
		hot[SUB] = temp && E_MASK(inst,21,0xF,0x2);
		hot[RSB] = temp && E_MASK(inst,21,0xF,0x3);
		hot[ADD] = temp && E_MASK(inst,21,0xF,0x4);
		hot[ADC] = temp && E_MASK(inst,21,0xF,0x5);
		hot[SBC] = temp && E_MASK(inst,21,0xF,0x6);
		hot[RSC] = temp && E_MASK(inst,21,0xF,0x7);
		hot[TST] = temp && E_MASK(inst,21,0xF,0x8) && E_MASK(inst, 20, 0x1, 0x1);
		hot[TEQ] = temp && E_MASK(inst,21,0xF,0x9) && E_MASK(inst, 20, 0x1, 0x1);
		hot[CMP] = temp && E_MASK(inst,21,0xF,0xA) && E_MASK(inst, 20, 0x1, 0x1);
		hot[CMN] = temp && E_MASK(inst,21,0xF,0xB) && E_MASK(inst, 20, 0x1, 0x1);
		hot[ORR] = temp && E_MASK(inst,21,0xF,0xC);
		hot[MOV] = temp && E_MASK(inst,21,0xF,0xD);
		hot[BIC] = temp && E_MASK(inst,21,0xF,0xE);
		hot[MVN] = temp && E_MASK(inst,21,0xF,0xF);

		hot[MRS]  = E_MASK(inst,23,0x1F,0x02) && E_MASK(inst,20,0x3,0x0) && E_MASK(inst,4,0xF,0x0);
		hot[MSR]  = E_MASK(inst,23,0x1F,0x02) && E_MASK(inst,20,0x3,0x2) && E_MASK(inst,4,0xF,0x0);
		hot[MSR] |= E_MASK(inst,23,0x1F,0x06) && E_MASK(inst,20,0x3,0x2);
		
		hot[MUL] = E_MASK(inst,21,0x7F,0x00) && E_MASK(inst,4,0xF,0x9);
		hot[MLA] = E_MASK(inst,21,0x7F,0x01) && E_MASK(inst,4,0xF,0x9);

		hot[UMULL] = E_MASK(inst,21,0x7F,0x04) && E_MASK(inst,4,0xF,0x9);
		hot[UMLAL] = E_MASK(inst,21,0x7F,0x05) && E_MASK(inst,4,0xF,0x9);
		hot[SMULL] = E_MASK(inst,21,0x7F,0x06) && E_MASK(inst,4,0xF,0x9);
		hot[SMLAL] = E_MASK(inst,21,0x7F,0x07) && E_MASK(inst,4,0xF,0x9);

		hot[SWP]  = E_MASK(inst,20,0xFF,0x10) && E_MASK(inst,4,0xF,0x9);
		hot[SWPB] = E_MASK(inst,20,0xFF,0x14) && E_MASK(inst,4,0xF,0x9);

		hot[BX] = E_MASK(inst,20,0xFF,0x12) && E_MASK(inst,4,0xF,0x1);

		hot[STRH]  = E_MASK(inst,25,0x7,0x0) && E_MASK(inst,20,0x1,0x0) && E_MASK(inst,4,0xF,0xB);
		hot[LDRH]  = E_MASK(inst,25,0x7,0x0) && E_MASK(inst,20,0x1,0x1) && E_MASK(inst,4,0xF,0xB);
		hot[LDRSB] = E_MASK(inst,25,0x7,0x0) && E_MASK(inst,20,0x1,0x1) && E_MASK(inst,4,0xF,0xD);
		hot[LDRSH] = E_MASK(inst,25,0x7,0x0) && E_MASK(inst,20,0x1,0x1) && E_MASK(inst,4,0xF,0xF);

		temp = E_MASK(inst,25,0x7,0x2) || (E_MASK(inst,25,0x7,0x3) && E_MASK(inst,4,0x1,0x0));
		hot[STR]   = temp && E_MASK(inst,20,0x05,0x00);
		hot[STRB]  = temp && E_MASK(inst,20,0x05,0x04);
		hot[STRT]  = temp && E_MASK(inst,20,0x17,0x02);
		hot[STRBT] = temp && E_MASK(inst,20,0x17,0x06);
		hot[LDR]   = temp && E_MASK(inst,20,0x05,0x01);
		hot[LDRB]  = temp && E_MASK(inst,20,0x05,0x05);
		hot[LDRT]  = temp && E_MASK(inst,20,0x17,0x03);
		hot[LDRBT] = temp && E_MASK(inst,20,0x17,0x07);
		hot[STR] = hot[STR] && !hot[STRT];
		hot[STRB] = hot[STRB] && !hot[STRBT];
		hot[LDR] = hot[LDR] && !hot[LDRT];
		hot[LDRB] = hot[LDRB] && !hot[LDRBT];


		hot[UND] = E_MASK(inst,25,0x7,0x3) && E_MASK(inst,4,0x1,0x1);

		hot[STM] = E_MASK(inst,25,0x7,0x4) && (E_MASK(inst,20,0x5,0x0) || E_MASK(inst,20,0x7,0x4));
		hot[LDM] = E_MASK(inst,25,0x7,0x4) && E_MASK(inst,20,0x1,0x1);

		hot[B]  = E_MASK(inst,24,0xF,0xA);
		hot[BL] = E_MASK(inst,24,0xF,0xB);

		hot[STC] = E_MASK(inst,25,0x7,0x6) && E_MASK(inst,20,0x1,0x0);
		hot[LDC] = E_MASK(inst,25,0x7,0x6) && E_MASK(inst,20,0x1,0x1);

		hot[CDP] = E_MASK(inst,24,0xF,0xE) && E_MASK(inst,4,0x1,0x0);

		hot[MCR] = E_MASK(inst,24,0xF,0xE) && E_MASK(inst,20,0x1,0x0) && E_MASK(inst,4,0x1,0x1);
		hot[MRC] = E_MASK(inst,24,0xF,0xE) && E_MASK(inst,20,0x1,0x1) && E_MASK(inst,4,0x1,0x1);

		hot[SWI] = E_MASK(inst,24,0xF,0xF);

		a=-1; b=0; c=-1;
		mux=0;
		for(j=0; j<NUM_ARM7INST; j++){
			if(hot[j]){
				if(a==-1){
					a = j;
				}else if(c==-1){
					c = j;
				}
				mux += (1L<<j);
				b++;
			}
		}
		switch(b){
			case 0:
				out[i] = UNK;
				// probably undefined... probably generates an undefined exception
				printf("!%03x, ",i);
				count2++;
				break;
			case 1:
				out[i] = a;
				// printf("%03x %s\n",i,ARM7INST_STRING[a]);
				break;
			default:
				printf("! %03x %d %013lx",i,b,mux);
				printf(" %s %s\n", ARM7INST_STRING[a], ARM7INST_STRING[c]);
				count++;
		}
	}

	printf("\nConflicts: %d, Unknowns: %d\n\n", count, count2);
}

#define NUM_ARM7INST_T 62
#define FOREACH_ARM7INST_T(Y) \
Y(TLSL1) Y(TLSR1) Y(TASR1) \
Y(TADD3) Y(TSUB3) Y(TADD1) Y(TMOV2) Y(TSUB1) \
Y(TMOV1) Y(TCMP1) Y(TADD2) Y(TSUB2) \
Y(TAND) Y(TEOR) Y(TLSL2) Y(TLSR2) Y(TASR2) Y(TADC) Y(TSBC) Y(TROR) \
Y(TTST) Y(TNEG) Y(TCMP2) Y(TCMN) Y(TORR) Y(TMUL) Y(TBIC) Y(TMVN) \
Y(TADD4) Y(TCMP3) Y(TMOV3) Y(TBX) \
Y(TLDR3) \
Y(TSTR2) Y(TSTRB2) Y(TLDR2) Y(TLDRB2) \
Y(TSTRH2) Y(TLDRSB) Y(TLDRH2) Y(TLDRSH) \
Y(TSTR1) Y(TLDR1) Y(TSTRB1) Y(TLDRB1) \
Y(TSTRH1) Y(TLDRH1) \
Y(TSTR3) Y(TLDR4) \
Y(TADD5) Y(TADD6) \
Y(TADD7) Y(TSUB4) \
Y(TPUSH) Y(TPOP) \
Y(TSTMIA) Y(TLDMIA) \
Y(TB1) \
Y(TSWI) Y(TB2) \
Y(TBL) \
Y(TUND) \
Y(TUNK)

enum ARM7INST_T_ENUM {
    FOREACH_ARM7INST_T(GENERATE_ENUM)
};

static const char *ARM7INST_T_STRING[] = {
    FOREACH_ARM7INST_T(GENERATE_STRING)
};

void p_decode_thumb_table(uint32_t *code){
	int i, c;

	printf("uint32_t decode_table_thumb[0x400] = {");
	for(i=0; i<0x400; i++){
		// if(i%256 == 0){
		// 	printf("\n\t// 0x%03x",i);
		// }
		if(i%16 == 0){
			printf("\n\t");
		}
		printf("%-6s,", ARM7INST_T_STRING[code[i]]);
		
	}
	printf("\n};\n");
}

void decode_thumb(uint32_t *out){
	uint32_t i, j;
	uint32_t inst;
	uint32_t hot[NUM_ARM7INST_T];
	uint64_t mux;

	int count, count2;
	int a, b, c;

	printf("*** THUMB DECODE ***\n");

	count=0; count2=0;
	for(i=0; i<0x400; i++){
		// inst[15:6] = i[9:0]
		inst = i<<6;

		hot[TLSL1] = E_MASK(inst,11,0x1F,0x00);
		hot[TLSR1] = E_MASK(inst,11,0x1F,0x01);
		hot[TASR1] = E_MASK(inst,11,0x1F,0x02);

		hot[TADD3] = E_MASK(inst,9,0x7F,0x0C);
		hot[TSUB3] = E_MASK(inst,9,0x7F,0x0D);
		hot[TADD1] = E_MASK(inst,9,0x7F,0x0E);
		hot[TMOV2] = E_MASK(inst,9,0x7F,0x0E) && E_MASK(inst,6,0x7,0x0);
		hot[TSUB1] = E_MASK(inst,9,0x7F,0x0F);

		hot[TMOV1] = E_MASK(inst,11,0x1F,0x04);
		hot[TCMP1] = E_MASK(inst,11,0x1F,0x05);
		hot[TADD2] = E_MASK(inst,11,0x1F,0x06);
		hot[TSUB2] = E_MASK(inst,11,0x1F,0x07);

		hot[TAND] = E_MASK(inst,6,0x3FF,0x100);
		hot[TEOR] = E_MASK(inst,6,0x3FF,0x101);
		hot[TLSL2] = E_MASK(inst,6,0x3FF,0x102);
		hot[TLSR2] = E_MASK(inst,6,0x3FF,0x103);
		hot[TASR2] = E_MASK(inst,6,0x3FF,0x104);
		hot[TADC] = E_MASK(inst,6,0x3FF,0x105);
		hot[TSBC] = E_MASK(inst,6,0x3FF,0x106);
		hot[TROR] = E_MASK(inst,6,0x3FF,0x107);
		hot[TTST] = E_MASK(inst,6,0x3FF,0x108);
		hot[TNEG] = E_MASK(inst,6,0x3FF,0x109);
		hot[TCMP2] = E_MASK(inst,6,0x3FF,0x10A);
		hot[TCMN] = E_MASK(inst,6,0x3FF,0x10B);
		hot[TORR] = E_MASK(inst,6,0x3FF,0x10C);
		hot[TMUL] = E_MASK(inst,6,0x3FF,0x10D);
		hot[TBIC] = E_MASK(inst,6,0x3FF,0x10E);
		hot[TMVN] = E_MASK(inst,6,0x3FF,0x10F);

		hot[TADD4] = E_MASK(inst,8,0xFF,0x44);
		hot[TCMP3] = E_MASK(inst,8,0xFF,0x45);
		hot[TMOV3] = E_MASK(inst,8,0xFF,0x46);
		hot[TBX] = E_MASK(inst,8,0xFF,0x47) && E_MASK(inst,7,0x1,0x0);

		hot[TLDR3] = E_MASK(inst,11,0x1F,0x09);

		hot[TSTR2] = E_MASK(inst,9,0x7F,0x28);
		hot[TSTRB2] = E_MASK(inst,9,0x7F,0x2A);
		hot[TLDR2] = E_MASK(inst,9,0x7F,0x2C);
		hot[TLDRB2] = E_MASK(inst,9,0x7F,0x2E);

		hot[TSTRH2] = E_MASK(inst,9,0x7F,0x29);
		hot[TLDRSB] = E_MASK(inst,9,0x7F,0x2B);
		hot[TLDRH2] = E_MASK(inst,9,0x7F,0x2D);
		hot[TLDRSH] = E_MASK(inst,9,0x7F,0x2F);

		hot[TSTR1] = E_MASK(inst,11,0x1F,0x0C);
		hot[TLDR1] = E_MASK(inst,11,0x1F,0x0D);
		hot[TSTRB1] = E_MASK(inst,11,0x1F,0x0E);
		hot[TLDRB1] = E_MASK(inst,11,0x1F,0x0F);

		hot[TSTRH1] = E_MASK(inst,11,0x1F,0x10);
		hot[TLDRH1] = E_MASK(inst,11,0x1F,0x11);

		hot[TSTR3] = E_MASK(inst,11,0x1F,0x12);
		hot[TLDR4] = E_MASK(inst,11,0x1F,0x13);

		hot[TADD5] = E_MASK(inst,11,0x1F,0x14);
		hot[TADD6] = E_MASK(inst,11,0x1F,0x15);

		hot[TADD7] = E_MASK(inst,8,0xFF,0xB0) && E_MASK(inst,7,0x1,0x0);
		hot[TSUB4] = E_MASK(inst,8,0xFF,0xB0) && E_MASK(inst,7,0x1,0x1);

		hot[TPUSH] = E_MASK(inst,9,0x7F,0x5A);
		hot[TPOP] = E_MASK(inst,9,0x7F,0x5E);

		hot[TSTMIA] = E_MASK(inst,11,0x1F,0x18);
		hot[TLDMIA] = E_MASK(inst,11,0x1F,0x19);

		hot[TB1] = E_MASK(inst,12,0xF,0xD);

		hot[TSWI] = E_MASK(inst,8,0xFF,0xDF);

		hot[TB2] = E_MASK(inst,11,0x1F,0x1C);

		hot[TBL] = E_MASK(inst,12,0xF,0xF);

		hot[TUND] = 0;
		// hot[TUND] |= E_MASK(inst,12,0xF,0x4) && E_MASK(inst,8,0xF,0x6);
		hot[TUND] |= E_MASK(inst,12,0xF,0x4) && E_MASK(inst,8,0xF,0x7) && E_MASK(inst,7,0x1,0x1);
		
		hot[TUND] |= E_MASK(inst,12,0xF,0xB) && E_MASK(inst,8,0xF,0x1);
		hot[TUND] |= E_MASK(inst,12,0xF,0xB) && E_MASK(inst,9,0x7,0x1);
		hot[TUND] |= E_MASK(inst,12,0xF,0xB) && E_MASK(inst,8,0xF,0x6) && E_MASK(inst,6,0x3,0x1);
		hot[TUND] |= E_MASK(inst,12,0xF,0xB) && E_MASK(inst,8,0xF,0x7);
		hot[TUND] |= E_MASK(inst,12,0xF,0xB) && E_MASK(inst,10,0x3,0x2);
		hot[TUND] |= E_MASK(inst,12,0xF,0xB) && E_MASK(inst,9,0x7,0x7);

		hot[TUND] |= E_MASK(inst,12,0xF,0xD) && E_MASK(inst,8,0xF,0xE);
		hot[TUND] |= E_MASK(inst,12,0xF,0xE) && E_MASK(inst,11,0x1,0x1);


		hot[TADD1] = hot[TADD1] && !hot[TMOV2];
		hot[TB1] = hot[TB1] && !hot[TUND];
		hot[TB1] = hot[TB1] && !hot[TSWI];

		a=-1; b=0; c=-1;
		mux=0;
		for(j=0; j<NUM_ARM7INST_T; j++){
			if(hot[j]){
				if(a==-1){
					a = j;
				}else if(c==-1){
					c = j;
				}
				mux += (1L<<j);
				b++;
			}
		}
		switch(b){
			case 0:
				out[i] = TUNK;
				// probably undefined... probably generates an undefined exception
				printf("!%04x, ",inst);
				count2++;
				break;
			case 1:
				out[i] = a;
				// printf("%03x %s\n",i,ARM7INST_T_STRING[a]);
				break;
			default:
				out[i] = a;
				printf("! %04x %d %09lx",inst,b,mux);
				printf(" %s %s\n", ARM7INST_T_STRING[a], ARM7INST_T_STRING[c]);
				count++;
		}
	}

	printf("\nConflicts: %d, Unknowns: %d\n\n", count, count2);
}

void p_enums(char *name, const char **names, int size){
	int i, n;

	n = 10;

	printf("#define %s_SIZE %d\n", name, size);

	// enum ARM7INST_ENUM {
	printf("enum %s_ENUM {", name);
	for(i=0; i<size; i++){
		if(i%10==0){
			printf("\n\t");
		}
		printf("%-7s,", names[i]);
	}
	printf("\n};\n");

	// static const char *ARM7INST_STRING[]
	printf("static const char %s_STRING[%s_SIZE] {", name, name);
	for(i=0; i<size; i++){
		if(i%10==0){
			printf("\n\t");
		}
		printf("\"%-7s\",", names[i]);
	}
	printf("\n};\n");
}

void p_decode_color(uint32_t *code, int size, int width, int max, int seed){
	uint32_t *palette;
	int height;
	int i;

	// set seed
	srand(seed);

	// generate palette
	palette = malloc(sizeof(uint32_t) * max);
	for(i=0; i<max; i++){
		palette[i] = ((rand()&0xF)<<8) + ((rand()&0xF)<<4) + (rand()&0xF);
	}

	// print image header, with 4 bit color per channel
	height = size / width;
	printf("P3 %d %d 16", width, height);

	// print image
	for(i=0; i<size; i++){
		if(i%width==0){
			printf("\n");
		}
		uint32_t color;
		color = palette[code[i]];
		printf("%2d %2d %2d ",(color>>8)&0xF,(color>>4)&0xF,color&0xF);
	}
	printf("\n");

	// free
	free(palette);
}

void main(void){
	uint32_t arm[0x1000];
	uint32_t thumb[0x400];
	decode(arm);
	decode_thumb(thumb);


	printf("*** DECODE TABLES ***\n");
	p_enums("ARM7INST",ARM7INST_STRING,NUM_ARM7INST);
	p_enums("ARM7INST_T",ARM7INST_T_STRING,NUM_ARM7INST_T);
	p_decode_table(arm);
	p_decode_thumb_table(thumb);

	printf("*** IMAGES ***\n");
	p_decode_color(arm,0x1000,128,NUM_ARM7INST,10);
	p_decode_color(arm,0x400,64,NUM_ARM7INST_T,12);
}