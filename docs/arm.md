
# ARMv4

	armv4 : 48
	adc, add  , and  , b   , bic  , bl  , cdp  , cmn  , cmp , eor ,
	ldc, ldm  , ldr  , ldrb, ldrbt, ldrh, ldrsb, ldrsh, ldrt, mcr ,
	mla, mov  , mrc  , mrs , msr  , mul , mvn  , orr  , rsb , rsc ,
	sbc, smlal, smull, stc , stm  , str , strb , strbt, strh, strt,
	sub, swi  , swp  , swpb, teq  , tst , umlal, umull
	
	armv4t : 1
	bx
	
	armv5t : 8
	bkpt, blx, cdp2, clz, ldc2, mcr2, mrc2, stc2
	
	armv5te (_, j, xp) : 15
	bxj, ldrd, mcrr, mrrc, pld, qadd, qdadd, qdsub, qsub, smla<x><y>,
	smlal<x><y>, smlaw<x><y>, smul<x><y>, smulw<y>, strd
	
	armv6 : 42
	cps, cpy, ldrex, mcrr2, mrrc2, pkh, qadd16, qadd8, qaddsubx, qsub16,
	qsub8, qsubaddx, rev, rfe, sadd, sel, setend, shadd, shsub, smlad,
	smlald,	smlsd, smlsld, smmla, smmls, smmul, smuad, smusd, srs, ssat,
	ssub, strex, sxt, uadd, uhadd, umaal, uqadd, uqsub, usad, usat,
	usub, uxt 

## Data processing

	generally :
	[27:26] == 00b

	specifically :
	[27:25] == 000b
	[4] == 0b
	or
	[27:25] == 000b
	[7] == 0b
	[4] == 1b
	or
	[27:25] == 001b

| instruction | [31:28] | [27:26] | [25] | [24:21] | [20] | [19:16] | [15:12] | [11:0] |
| - | - | - | - | - | - | - | - | - |
| AND | cond | 00 | I | 0000 | S | Rn | Rd | shifter_operand |
| EOR | cond | 00 | I | 0001 | S | Rn | Rd | shifter_operand |
| SUB | cond | 00 | I | 0010 | S | Rn | Rd | shifter_operand |
| RSB | cond | 00 | I | 0011 | S | Rn | Rd | shifter_operand |
| ADD | cond | 00 | I | 0100 | S | Rn | Rd | shifter_operand |
| ADC | cond | 00 | I | 0101 | S | Rn | Rd | shifter_operand |
| SBC | cond | 00 | I | 0110 | S | Rn | Rd | shifter_operand |
| RSC | cond | 00 | I | 0111 | S | Rn | Rd | shifter_operand |
| TST | cond | 00 | I | 1000 | 1 | Rn | SBZ | shifter_operand |
| TEQ | cond | 00 | I | 1001 | 1 | Rn | SBZ | shifter_operand |
| CMP | cond | 00 | I | 1010 | 1 | Rn | SBZ | shifter_operand |
| CMN | cond | 00 | I | 1011 | 1 | Rn | SBZ | shifter_operand |
| ORR | cond | 00 | I | 1100 | S | Rn | Rd | shifter_operand |
| MOV | cond | 00 | I | 1101 | S | SBZ | Rd | shifter_operand |
| BIC | cond | 00 | I | 1110 | S | Rn | Rd | shifter_operand |
| MVN | cond | 00 | I | 1111 | S | SBZ | Rd | shifter_operand |

## PSR transfer

	generally :
	[27:26] == 00b
	[24:23] == 10b
	[20] == 0b
	
	specifically :
	[27:23] == 00010b
	[21:20] == 00b
	[7:4] == 0000b ?
	or
	[27:23] == 00010b
	[21:20] == 10b
	[7:4] == 0000b ?
	or
	[27:23] == 00110b
	[21:20] == 10b
		

| instruction | [31:28] | [27:23] | [22] | [21:20] | [19:16] | [15:12] | [11:8] | [7:0] |
| - | - | - | - | - | - | - | - | - |
| MRS | cond | 00010 | R | 00 | SBO | Rd | SBZ | 0000 SBZ |
| MSR (reg) | cond | 00010 | R | 10 | field_mask | SBO | SBZ | 0000 Rm |
| MSR (imm) | cond | 00110 | R | 10 | field_mask | SBO | rotate_imm | 8_bit_immediate |

## Multiply

	[27:22] == 000000b
	[7:4] == 1001b

| instruction | [31:28] | [27:21] | [20] | [19:16] | [15:12] | [11:8] | [7:4] | [3:0] |
| - | - | - | - | - | - | - | - | - |
| MUL | cond | 0000000 | S | Rd | SBZ | Rs | 1001 | Rm |
| MLA | cond | 0000001 | S | Rd | Rn | Rs | 1001 | Rm |

## Multiply long

	[27:23] == 00001b
	[7:4] == 1001b

| instruction | [31:28] | [27:21] | [20] | [19:16] | [15:12] | [11:8] | [7:4] | [3:0] |
| - | - | - | - | - | - | - | - | - |
| UMULL | cond | 0000100 | S | RdHi | RdLo | Rs | 1001 | Rm |
| UMLAL | cond | 0000101 | S | RdHi | RdLo | Rs | 1001 | Rm |
| SMULL | cond | 0000110 | S | RdHi | RdLo | Rs | 1001 | Rm |
| SMLAL | cond | 0000111 | S | RdHi | RdLo | Rs | 1001 | Rm |

## Single data swap

	[27:23] == 00010b
	[21:20] == 00b
	[11:8] SBZ
	[7:4] == 1001b

| instruction | [31:28] | [27:23] | [22] | [21:20] | [19:16] | [15:12] | [11:8] | [7:4] |[3:0] |
| - | - | - | - | - | - | - | - | - | - |
| SWP | cond | 00010 | 0 | 00 | Rn | Rd | SBZ | 1001 | Rm |
| SWPB | cond | 00010 | 1 | 00 | Rn | Rd | SBZ | 1001 | Rm |

## Branch and exchange

	[27:20] == 00010010b
	[19:8] SBO
	[7:4] == 0001b

| instruction | [31:28] | [27:20] | [19:8] | [7:4] | [3:0] |
| - | - | - | - | - | - |
| BX | cond | 00010010 | SBO | 0001 | Rm |

## Halfword data transfer

	[27:25] == 000b
	[7] == 1b
	[4] == 1b

| instruction | [31:28] | [27:25] | [24] | [23] | [22] | [21] | [20] | [19:16] | [15:12] | [11:8] | [7:4] | [3:0] |
| - | - | - | - | - | - | - | - | - | - | - | - | - |
| STRH | cond | 000 | P | U | I | W | 0 | Rn | Rd | addr_mode | 1011 | addr_mode |
| LDRH | cond | 000 | P | U | I | W | 1 | Rn | Rd | addr_mode | 1011 | addr_mode |
| LDRSB | cond | 000 | P | U | I | W | 1 | Rn | Rd | addr_mode | 1101 | addr_mode |
| LDRSH | cond | 000 | P | U | I | W | 1 | Rn | Rd | addr_mode | 1111 | addr_mode |

## Halfword data transfer, register offset

	[27:25] == 000b
	[22] == 0b
	[11:8] SBZ
	[7] == 1b
	[4] == 1b

| instruction | [31:28] | [27:25] | [24] | [23] | [22] | [21] | [20] | [19:16] | [15:12] | [11:8] | [7:4] | [3:0] |
| - | - | - | - | - | - | - | - | - | - | - | - | - |
| STRH | cond | 000 | P | U | 0 | W | 0 | Rn | Rd | SBZ | 1011 | Rm |
| LDRH | cond | 000 | P | U | 0 | W | 1 | Rn | Rd | SBZ | 1011 | Rm |
| LDRSB | cond | 000 | P | U | 0 | W | 1 | Rn | Rd | SBZ | 1101 | Rm |
| LDRSH | cond | 000 | P | U | 0 | W | 1 | Rn | Rd | SBZ | 1111 | Rm |

## Halfword data transfer, immediate offset

	[27:25] == 000b
	[22] == 1b
	[7] == 1b
	[4] == 1b

| instruction | [31:28] | [27:25] | [24] | [23] | [22] | [21] | [20] | [19:16] | [15:12] | [11:8] | [7:4] | [3:0] |
| - | - | - | - | - | - | - | - | - | - | - | - | - |
| STRH | cond | 000 | P | U | 1 | W | 0 | Rn | Rd | immedH | 1011 | immedL |
| LDRH | cond | 000 | P | U | 1 | W | 1 | Rn | Rd | immedH | 1011 | immedL |
| LDRSB | cond | 000 | P | U | 1 | W | 1 | Rn | Rd | immedH | 1101 | immedL |
| LDRSH | cond | 000 | P | U | 1 | W | 1 | Rn | Rd | immedH | 1111 | immedL |

## Single data transfer

	generally :
	[27:26] == 01b
	
	specifically :
	[27:25] == 010b
	or
	[27:25] == 011b
	[4] == 0b

| instruction | [31:28] | [27:26] | [25] | [24] | [23] | [22] | [21] | [20] | [19:16] | [15:12] | [11:0] |
| - | - | - | - | - | - | - | - | - | - | - | - |
| STR | cond | 01 | I | P | U | 0 | W | 0 | Rn | Rd | addr_mode |
| STRB | cond | 01 | I | P | U | 1 | W | 0 | Rn | Rd | addr_mode |
| STRT | cond | 01 | I | 0 | U | 0 | 1 | 0 | Rn | Rd | addr_mode |
| STRBT | cond | 01 | I | 0 | U | 1 | 1 | 0 | Rn | Rd | addr_mode |
| LDR | cond | 01 | I | P | U | 0 | W | 1 | Rn | Rd | addr_mode |
| LDRB | cond | 01 | I | P | U | 1 | W | 1 | Rn | Rd | addr_mode |
| LDRT | cond | 01 | I | 0 | U | 0 | 1 | 1 | Rn | Rd | addr_mode |
| LDRBT | cond | 01 | I | 0 | U | 1 | 1 | 1 | Rn | Rd | addr_mode |

## Undefined

	[27:25] == 011b
	[4] = 1b

## Block data transfer

	[27:25] == 100b

| instruction | [31:28] | [27:25] | [24] | [23] | [22] | [21] | [20] | [19:16] | [15:0] |
| - | - | - | - | - | - | - | - | - | - |
| STM (1) | cond | 100 | P | U | 0 | W | 0 | Rn | register_list |
| STM (2) | cond | 100 | P | U | 1 | 0 | 0 | Rn | register_list |
| LDM (1) | cond | 100 | P | U | 0 | W | 1 | Rn | register_list |
| |

| instruction | [31:28] | [27:25] | [24] | [23] | [22] | [21] | [20] | [19:16] | [15] | [14:0] |
| - | - | - | - | - | - | - | - | - | - | - |
| LDM (2) | cond | 100 | P | U | 1 | 0 | 1 | Rn | 0 | register_list |
| LDM (3) | cond | 100 | P | U | 1 | W | 1 | Rn | 1 | register_list |

## Branch

	[27:25] == 101b

| instruction | [31:28] | [27:25] | [24] | [23:0] |
| - | - | - | - | - |
| B, BL | cond | 101 | L | signed_immed_24 |

## Coprocessor data transfer

	[27:25] == 110b

| instruction | [31:28] | [27:25] | [24] | [23] | [22] | [21] | [20] | [19:16] | [15:12] | [11:8] | [7:0] |
| - | - | - | - | - | - | - | - | - | - | - | - |
| STC | cond | 110 | P | U | N | W | 0 | Rn | CRd | cp_num | 8_bit_word_offset |
| LDC | cond | 110 | P | U | N | W | 1 | Rn | CRd | cp_num | 8_bit_word_offset |

## Coprocessor data operation

	[27:24] == 1110b
	[4] == 0b

| instruction | [31:28] | [27:24] | [23:20] | [19:16] | [15:12] | [11:8] | [7:5] | [4] | [3:0] |
| - | - | - | - | - | - | - | - | - | - |
| CDP | cond | 1110 | opcode_1 | CRn | CRd | cp_num | opcode_2 | 0 | CRm |

## Coprocessor register transfer

	[27:24] == 1110b
	[4] == 1b

| instruction | [31:28] | [27:24] | [23:21] | [20] | [19:16] | [15:12] | [11:8] | [7:5] | [4] | [3:0] |
| - | - | - | - | - | - | - | - | - | - | - |
| MCR | cond | 1110 | opcode_1 | 0 | CRn | Rd | cp_num | opcode_2 | 1 | CRm |
| MRC | cond | 1110 | opcode_1 | 1 | CRn | Rd | cp_num | opcode_2 | 1 | CRm |

## Software interrupt

	[27:24] == 1111b

| instruction | [31:28] | [27:24] | [23:0] |
| - | - | - | - |
| SWI | cond | 1111 | immed_24 |

##



## ARMv5+
| instruction |
| - |
| BKPT |
| BLX (1) |
| BLX (2) |
| BXJ |
| CDP2 |
| CLZ |
| LDC2 |
| MCR2 |
| MCRR |
| MRC2 |
| MRRC |
| PLD |
| QADD |
| QDADD |
| QDSUB |
| QSUB |
| SMLA(BB,BT,TB,TT) |
| SMLAL(BB,BT,TB,TT) |
| SMLAW(B,T) |
| SMUL(BB,BT,TB,TT) |
| SMULW(B,T) |
| STC2 |

##
| instruction | [31:28] | [27:25] | [24] | [23] | [22] | [21] | [20] | [19:16] | [15:12] | [11:8] | [7:4] | [3:0] |
| - | - | - | - | - | - | - | - | - | - | - | - | - |
| LDRD | cond | 000 | P | U | I | W | 0 | Rn | Rd | addr_mode | 1101 | addr_mode |
| STRD | cond | 000 | P | U | I | W | 0 | Rn | Rd | addr_mode | 1111 | addr_mode |

## ARMv6+
| instruction |
| - |
| CPS |
| CPY |
| LDREX |
| MCRR2 |
| MRRC2 |
| PKHBT |
| PKHTB |
| QADD16 |
| QADD8 |
| QADDSUBX |
| QSUB16 |
| QSUB8 |
| QSUBADDX |
| REV |
| REV16 |
| REVSH |
| RFE |
| SADD16 |
| SADD8 |
| SADDSUBX |
| SEL |
| SETEND |
| SHADD16 |
| SHADD8 |
| SHADDSUBX |
| SHSUB16 |
| SHSUB8 |
| SHSUBADDX |
| SMLAD |
| SMLALD |
| SMLSD |
| SMLSLD |
| SMMLA |
| SMMLS |
| SMMUL |
| SMUAD |
| SMUSD |
| SRS |
| SSAT |
| SSAT16 |
| SSUB16 |
| SSUB8 |
| SSUBADDX |
| STREX |
| SXTAB |
| SXTAB16 |
| SXTAH |
| SXTB |
| SXTB16 |
| SXTH |
| UADD16 |
| UADD8 |
| UADDSUBX |
| UHADD16 |
| UHADD8 |
| UHADDSUBX |
| UHSUB16 |
| UHSUB8 |
| UHSUBADDX |
| UMAAL |
| UQADD16 |
| UQADD8 |
| UQADDSUBX |
| UQSUB16 |
| UQSUB8 |
| UQSUBADDX |
| USAD8 |
| USADA8 |
| USAT |
| USAT16 |
| USUB16 |
| USUB8 |
| USUBADDX |
| UXTAB |
| UXTAB16 |
| UXTAH |
| UXTB |
| UXTB16 |
| UXTH |

