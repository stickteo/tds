# THUMBv1

	armv4 : 35
	adc, add, and, asr, b, bic, bl, bx,
	cmn, cmp, eor, ldmia, ldr, ldrb, ldrh, ldrsb,
	ldrsh, lsl, lsr, mov, mul, mvn, neg, orr,
	pop, push, ror, sbc, stmia, str, strb, strh,
	sub, swi, tst

	armv5 : 2
	bkpt, blx

	armv6 : 10
	cps, cpy, rev, rev16, revsh, setend, sxtb, sxth,
	uxtb, uxth

## 01 : Move shifted register

	[15:13] == 000b

| instruction | [15:11] | [10:6] | [5:3] | [2:0] |
| - | - | - | - | - |
| LSL (1) | 000 00 | immed_5 | Rm | Rd |
| LSR (1) | 000 01 | immed_5 | Rm | Rd |
| ASR (1) | 000 10 | immed_5 | Rm | Rd |

## 02 : Add and subtract

	[15:11] == 00011b

| instruction | [15:9] | [8:6] | [5:3] | [2:0] |
| - | - | - | - | - |
| ADD (3) | 00011 00 | Rm | Rn | Rd |
| SUB (3) | 00011 01 | Rm | Rn | Rd |
| ADD (1) | 00011 10 | immed_3 | Rn | Rd |
| MOV (2) | 00011 10 | 000 | Rn | Rd |
| SUB (1) | 00011 11 | immed_3 | Rn | Rd |

## 03 : Move, compare, add, and subtract immediate

	[15:13] == 001b
	
| instruction | [15:11] | [10:8] | [7:0]  |
| - | - | - | - |
| MOV (1) | 001 00 | Rd | immed_8 |
| CMP (1) | 001 01 | Rn | immed_8 |
| ADD (2) | 001 10 | Rd | immed_8 |
| SUB (2) | 001 11 | Rd | immed_8 |


## 04 : ALU operation

	[15:10] == 010000b

| instruction | [15:10] | [9:6] | [5:3] | [2:0] |
| - | - | - | - | - |
| AND | 010000 | 0000 | Rm | Rd |
| EOR | 010000 | 0001 | Rm | Rd |
| LSL (2) | 010000 | 0010 | Rs | Rd |
| LSR (2) | 010000 | 0011 | Rs | Rd |
| ASR (2) | 010000 | 0100 | Rs | Rd |
| ADC | 010000 | 0101 | Rm | Rd |
| SBC | 010000 | 0110 | Rm | Rd |
| ROR | 010000 | 0111 | Rs | Rd |
| TST | 010000 | 1000 | Rm | Rn |
| NEG | 010000 | 1001 | Rm | Rd |
| CMP (2) | 010000 | 1010 | Rm | Rn |
| CMN | 010000 | 1011 | Rm | Rn |
| ORR | 010000 | 1100 | Rm | Rd |
| MUL | 010000 | 1101 | Rm | Rd |
| BIC | 010000 | 1110 | Rm | Rd |
| MVN | 010000 | 1111 | Rm | Rd |

## 05 : High register operations and branch exchange

	[15:10] == 010001b

| instruction | [15:8] | [7] | [6] | [5:3] | [2:0] |
| - | - | - | - | - | - |
| ADD (4) | 010001 00 | H1 | H2 | Rm | Rd |
| CMP (3) | 010001 01 | H1 | H2 | Rm | Rn |
| MOV (3) | 010001 10 | H1 | H2 | Rm | Rd |
| BX | 010001 11 | 0 | H2 | Rm | SBZ |

## 06 : PC-relative load

	[15:11] == 01001b

| instruction | [15:11] | [10:8] | [7:0]  |
| - | - | - | - |
| LDR (3) | 01001 | Rd | immed_8 |

## 07 : Load and store with relative offset

	[15:12] == 0101b
	[9] == 0b

| instruction | [15:9] | [8:6] | [5:3] | [2:0] |
| - | - | - | - | - |
| STR (2) | 0101 00 0 | Rm | Rn | Rd |
| STRB (2) | 0101 01 0 | Rm | Rn | Rd |
| LDR (2) | 0101 10 0 | Rm | Rn | Rd |
| LDRB (2) | 0101 11 0 | Rm | Rn | Rd |

## 08 : Load and store sign-extended byte and halfword

	[15:12] == 0101b
	[9] == 1b

| instruction | [15:9] | [8:6] | [5:3] | [2:0] |
| - | - | - | - | - |
| STRH (2) | 0101 00 1 | Rm | Rn | Rd |
| LDRSB | 0101 01 1 | Rm | Rn | Rd |
| LDRH (2) | 0101 10 1 | Rm | Rn | Rd |
| LDRSH | 0101 11 1 | Rm | Rn | Rd |

## 09 : Load and store with immediate offset

	[15:13] == 011b

| instruction | [15:11] | [10:6] | [5:3] | [2:0] |
| - | - | - | - | - |
| STR (1) | 011 00 | immed_5 | Rn | Rd |
| LDR (1) | 011 01 | immed_5 | Rn | Rd |
| STRB (1) | 011 10 | immed_5 | Rn | Rd |
| LDRB (1) | 011 11 | immed_5 | Rn | Rd |

## 10 : Load and store halfword

	[15:12] == 1000b

| instruction | [15:11] | [10:6] | [5:3] | [2:0] |
| - | - | - | - | - |
| STRH (1) | 1000 0 | immed_5 | Rn | Rd |
| LDRH (1) | 1000 1 | immed_5 | Rn | Rd |

## 11 : SP-relative load and store

	[15:12] == 1001b

| instruction | [15:11] | [10:8] | [7:0]  |
| - | - | - | - |
| STR (3) | 1001 0 | Rd | immed_8 |
| LDR (4) | 1001 1 | Rd | immed_8 |

## 12 : Load address

	[15:12] == 1010b

| instruction | [15:11] | [10:8] | [7:0]  |
| - | - | - | - |
| ADD (5) | 1010 0 | Rd | immed_8 |
| ADD (6) | 1010 1 | Rd | immed_8 |

## 13 : Add offset to stack pointer

	[15:8] == 10110000b

| instruction | [15:7] | [6:0] |
| - | - | - |
| ADD (7) | 10110000 0 | immed_7 |
| SUB (4) | 10110000 1 | immed_7 |

## 14 : Push and pop registers

	[15:12] == 1011b
	[10:9] == 10b


| instruction | [15:9] | [8] | [7:0] |
| - | - | - | - |
| PUSH | 1011 0 10 | R | register_list |
| POP | 1011 1 10 | R | register_list |

## 15 : Multiple load and store

	[15:12] == 1100b

| instruction | [15:11] | [10:8] | [7:0]  |
| - | - | - | - |
| STMIA | 1100 0 | Rn | register_list |
| LDMIA | 1100 1 | Rn | register_list |

## 16 : Conditional branch

	[15:12] == 1101b

| instruction | [15:12] | [11:8] | [7:0] |
| - | - | - | - |
| B (1) | 1101 | cond | signed_immed_8 |

## 17 : Software interrupt

	[15:8] == 11011111b

| instruction | [15:8] | [7:0] |
| - | - | - |
| SWI | 11011111 | immed_8 |

## 18 : Unconditional branch

	[15:11] == 11100b

| instruction | [15:11] | [10:0] |
| - | - | - |
| B (2) | 11100 | signed_immed_11 |

## 19 : Long branch with link

	[15:12] == 1111b

| instruction | [15:12] | [11] | [10:0] |
| - | - | - | - |
| BL | 1111 | H | offset_11 |

## Undefined
| [15:12] | [11:8] | [7:6] | [5:0]
| - | - | - | - |
| 1011 | 0001 |
| 1011 | 0_11 |
| 1011 | 100_ |
| 1011 | 1010 | 10 |
| 1011 | 1011 |
| 1011 | 1111 |
||
| 1101 | 1110 |

### ARMv5+
| instruction | [15:12] | [11:8] | [7:6] | [5:0] |
| - | - | - | - | - |
| BLX (2) | 0100 | 0111 | 1_ |
| BKPT | 1011 | 1110 |
| BLX (1) | 1110 | 1___ |

### ARMv6+
| instruction | [15:12] | [11:8] | [7:6] | [5:0] |
| - | - | - | - | - |
| CPY | 0100 | 0110 |
| SXTH | 1011 | 0010 | 00 |
| SXTB | 1011 | 0010 | 01 |
| UXTH | 1011 | 0010 | 10 |
| UXTB | 1011 | 0010 | 11 |
| SETEND | 1011 | 0110 | 01 | 01____ |
| CPS | 1011 | 0110 | 01 | 1_0___ |
| REV | 1011 | 1010 | 00 |
| REV16 | 1011 | 1010 | 01 |
| REVSH | 1011 | 1010 | 11 |

### Together
| instruction | [15:12] | [11:8] | [7:6] | [5:0] |
| - | - | - | - | - |
| BLX (2) | 0100 | 0111 | 1_ |
| CPY | 0100 | 0110 |
||
| | 1011 | 0001 |
| SXTH | 1011 | 0010 | 00 |
| SXTB | 1011 | 0010 | 01 |
| UXTH | 1011 | 0010 | 10 |
| UXTB | 1011 | 0010 | 11 |
| | 1011 | 0_11 |
| SETEND | 1011 | 0110 | 01 | 01____ |
| CPS | 1011 | 0110 | 01 | 1_0___ |
| | 1011 | 100_ |
| REV | 1011 | 1010 | 00 |
| REV16 | 1011 | 1010 | 01 |
| | 1011 | 1010 | 10 |
| REVSH | 1011 | 1010 | 11 |
| BKPT | 1011 | 1110 |
| | 1011 | 1011 |
| | 1011 | 1111 |
||
| | 1101 | 1110 |
||
| BLX (1) | 1110 | 1___ |

### Compressed
| [15:12] | [11:8] | [7:6] | [5:0] |
| - | - | - | - |
| 0100 | 0110 |
| 0100 | 0111 | 1_ |
| 1011 | 0001 |
| 1011 | 001_ |
| 1011 | 0110 | 01 |
| 1011 | 0111 |
| 1011 | 10__ |
| 1011 | 111_ |
| 1101 | 1110 |
| 1110 | 1___ |

## ARMv5+

| instruction | [15:8] | [7:0] |
| - | - | - |
| BKPT | 10111110 | immed_8 |
||

| instruction | [15:7] | [6] | [5:3] | [2:0] |
| - | - | - | - | - |
| BLX (2) | 010001111 | H2 |
||

| instruction | [15:13] | [12:11] | [10:0] |
| - | - | - | - |
| BLX (1) | 111 | 01 | offset_11 |


## ARMv6+

| instruction |
| - |
| CPS |
| CPY |
| REV |
| REV16 |
| REVSH |
| SETEND |
| SXTB |
| SXTH |
| UXTB |
| UXTH |












