# tDS

another nds emulator... a fun project exploring how to program an emulator from scratch and using available documentation...

## useful links

ARM ARM:
<https://www.scss.tcd.ie/%7Ewaldroj/3d1/arm_arm.pdf>

ARM7TDMI:
<http://infocenter.arm.com/help/topic/com.arm.doc.ddi0210c/DDI0210B.pdf>

ARM946E-S:
<http://infocenter.arm.com/help/topic/com.arm.doc.ddi0201d/DDI0201D_arm946es_r1p1_trm.pdf>

GBATEK:
<https://problemkaputt.de/gbatek.htm>

